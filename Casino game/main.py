def calculate_earning(number_picked, money_bet, winning_number, wallet):
    """
    Add or remove money depending on results
    :param number_picked: int [0:50[
    :param money_bet: float
    :param winning_number: int [0:50[
    """
    if number_picked == winning_number:
        wallet += money_bet*3
        print("You won ", str(money_bet*3), "$")
    else:
        print("The winning number was ", winning_number)
        if winning_number % 2 == number_picked % 2:
            wallet += money_bet*0.5
            print("You won ", str(money_bet*0.5), "$")
        else:
            wallet -= money_bet
            print("You lose ", money_bet, "$")

    return wallet


def pick_number():
    """
    Allow the player to choose a case
    With basic integer range verification
    :return: 0<int<50
    """
    num_picked = -1
    while num_picked < 0 or num_picked > 49:
        num_picked = input("What number are you betting on? ")
        try:
            num_picked = int(num_picked)
            if num_picked < 0 or num_picked > 49:
                print("There is only 50 cases!")
            else:
                print("You choose ", num_picked)
                return num_picked
        except ValueError:
            print("Retry")


def bet(wallet):
    """
    How many the player wants to bet
    With valid number verification
    """
    money = False
    while money is not True:
        money_invested = input("how much do you want to bet? ")
        try:
            money_invested = float(money_invested)
            if money_invested > wallet or money_invested <= 0:
                print("You don't have enough on your wallet!")
            else:
                money = True
                print("You bet ", money_invested, " $")
                return money_invested
        except ValueError:
            print("You have to input a number!")
            pass


def winning_num():
    """
    Randomly choose a number which is the winning number
    :return: int
    """
    import random
    return random.randrange(0, 50)


def main():
    """
    Main part
    :return:
    """
    lost = False

    global wallet
    wallet = 1000.0

    while not lost:
        number_picked = pick_number()
        money_bet = bet(wallet)
        winning_number = winning_num()  # number pick = bet *3
        wallet = calculate_earning(number_picked, money_bet, winning_number, wallet)
        print(" Current wallet : ", wallet, "$")
        print("---------------------")
        if wallet <= 0:
            lost = True
            print("YOU LOST")
        else:
            lost = False


if __name__ == "__main__":
    main()
