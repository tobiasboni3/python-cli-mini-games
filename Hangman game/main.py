import functions
import data


def main():
    count = 0
    word_to_find = functions.generate_a_word()
    l_word_to_find = functions.decompose(word_to_find)
    hidden_word = functions.generate_fake_word(word_to_find)
    print("Input 'exit' to leave the game ")
    while '*' in hidden_word:
        if count >= data.allowed_count:
            print("Word was ", word_to_find)
            return
        else:
            if functions.input_letter() is False:  # exiting
                return
            hidden_word = functions.letter_analysis(functions.input_letter(), l_word_to_find, hidden_word)
            count += 1
    result = functions.recompose(hidden_word)

    print("---------------------")
    print("The results is ", result)
    print("Count: ", count)

    return


if __name__ == '__main__':
    main()
