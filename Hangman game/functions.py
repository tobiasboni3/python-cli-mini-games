def generate_a_word():
    import random
    import data
    return data.list_of_words[random.randrange(0, len(data.list_of_words))]


def decompose(word):
    l = list()
    for i in range(len(word)):
        l += word[i]
    # print("debug decompose", l)
    return l


def generate_fake_word(word):
    out = str()
    for i in range(len(word)):
        out += "*"
    print("Hidden word: ", out)
    return list(out)


def input_letter():
    try:
        letter = input("Input a letter: ")
        if "exit" in letter.lower():
            print("Normal exiting")
            return False
    except KeyboardInterrupt:
        print("Try again...")
        print("Press c to exit")
        return None
    return letter[:1]


def letter_analysis(letter_inputed, word_to_find_list, hide_word):
    hide_word = hide_word
    for i in range(len(word_to_find_list)):
        if letter_inputed == word_to_find_list[i]:
            hide_word[i] = hide_word[i].replace("*", letter_inputed)
    hide_word_str = str()
    for i in range(len(hide_word)):
        hide_word_str += hide_word[i]
    print(hide_word_str)
    return hide_word


def recompose(hidden_word):
    r = str()
    for i in range(len(hidden_word)):
        r += hidden_word[i]
    return r
